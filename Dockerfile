################################################################################
# Default Registry Var
FROM registry.gitlab.com/shawnstephens517/stig_rhel8_container:master

ARG NGINX_VERSION="1.19.0"

################################################################################

################################################################################
# Base Variables
EXPOSE 8080
EXPOSE 8443

################################################################################
# Container Metadata
ENV SUMMARY="Platform for running nginx $NGINX_VERSION on ubi8" \
    DESCRIPTION="Nginx is a web server and a reverse proxy server for HTTP, SMTP, POP3 and IMAP \
protocols, with a strong focus on high concurrency, performance and low memory usage. The container \
image provides a containerized packaging of the nginx $NGINX_VERSION daemon. The image can be used \
as a base image for other applications based on nginx $NGINX_VERSION web server. \
Nginx server image can be extended using source-to-image tool."

LABEL summary="${SUMMARY}" \
      description="${DESCRIPTION}" \
      version="${NGINX_VERSION}"

################################################################################
# Yum Install Packages


RUN set -x && \
    curl -o /tmp/nginx-1.19.0.rpm https://nginx.org/packages/mainline/rhel/8/x86_64/RPMS/nginx-1.19.0-1.el8.ngx.x86_64.rpm && \
    curl -o /tmp/nginx_signing.key https://nginx.org/keys/nginx_signing.key && \
    rpm --import /tmp/nginx_signing.key && \
    yum update -y && \
    yum localinstall -y /tmp/nginx-1.19.0.rpm && \
    yum clean all

RUN rm /tmp/nginx-1.19.0.rpm

################################################################################
# Final Configs
USER nginx
HEALTHCHECK CMD curl --fail http://localhost:8080 || exit 1  

CMD ["nginx", "-g", "daemon off;"]
